# Плейбук для развертывания тестового приложения на Django (Задание #230501).

* Ansible Playbook для установки нужных сервисов и мониторинга (Gunicorn exporter, PostgresSQL exporter, NGINX Prometheus Exporter, Node exporter) находится в папке Ansible. Все роли подключаемые роли разрабатываются в отдельных репозиториях, покрыты тестами Molecule. Установка осуществляется из файла requirements.yml посредством ansible-galaxy. Реализована установка через GitLab CI/CD Pipeline.

* Пример использования Ansible Playbook
```
- ansible-galaxy install -r requirements.yml
- ansible-playbook main.yml -i ./inventory/Dev/hosts
```

* Пример playbook
```
- hosts: 
    - web
  roles:
    - sample.nginx
    - sample.prometheus_exporter
  become: true  
- hosts:
    - db
  roles:
    - sample.postgresql
    - sample.prometheus_exporter
  become: true  
- hosts:
    - app
  roles:
    - sample.django
    - sample.supervisord
    - sample.prometheus_exporter
  become: true  
```

